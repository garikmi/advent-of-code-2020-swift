import Foundation

let manager = FileManager.default
var numbers = [Int]()

if let url = manager.urls(for: .desktopDirectory, in: .userDomainMask).first {
    let inputFile = url.appendingPathComponent("input").appendingPathExtension("txt")

    do {
        let savedData = try String(contentsOf: inputFile)
        let tempArray = savedData.components(separatedBy: "\n")
        numbers = tempArray.map { Int($0)! } // note: all values must be convertable to Int, otherwise error
    } catch {
        print(error.localizedDescription)
    }
}

var result = 0
for index in numbers.indices {
    if let secondIndex = numbers.firstIndex(of: 2020 - numbers[index]) {
        result = numbers[index] * numbers[secondIndex]
    }
}

print(result)
