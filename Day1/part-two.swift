import Foundation

let manager = FileManager.default
var numbers = [Int]()

if let url = manager.urls(for: .desktopDirectory, in: .userDomainMask).first {
    let inputFile = url.appendingPathComponent("input").appendingPathExtension("txt")

    do {
        let savedData = try String(contentsOf: inputFile)
        let tempArray = savedData.components(separatedBy: "\n")
        numbers = tempArray.map { Int($0)! } // note: all values must be convertable to Int, otherwise error
    } catch {
        print(error.localizedDescription)
    }
}

var result = 0
outerLoop: for index in numbers.indices {
    for secondIndex in index..<numbers.count {
        if numbers[index] + numbers[secondIndex] < 2020 {
            if let thirdIndex = numbers.firstIndex(of: 2020 - (numbers[index] + numbers[secondIndex])) {
                print(numbers[index] * numbers[secondIndex] * numbers[thirdIndex])
                break outerLoop
            }
        }
    }
}

print(result)
